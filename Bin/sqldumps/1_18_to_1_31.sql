ALTER TABLE `accounts`
	ADD COLUMN `email` VARCHAR(64) NULL DEFAULT '' AFTER `created_ip_address`;

ALTER TABLE `accounts`
	ADD COLUMN `emailsetused` BIT(1) NOT NULL DEFAULT b'0' AFTER `email`;

ALTER TABLE `accounts`
	ADD COLUMN `banned` TINYINT(4) NOT NULL DEFAULT '0' AFTER `emailsetused`;
	  

ALTER TABLE `characters`
	ADD COLUMN `ts_deleted` INT(11) NOT NULL DEFAULT '0' AFTER `instance_ts`,
	ADD COLUMN `ts_login` INT(11) NOT NULL DEFAULT '0' AFTER `ts_deleted`;

CREATE TABLE `character_squelch` (
	`character_id` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`squelched_id` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`account_id` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`isip` BIT(1) NULL DEFAULT b'0',
	`isspeech` BIT(1) NULL DEFAULT b'0',
	`istell` BIT(1) NULL DEFAULT b'0',
	`iscombat` BIT(1) NULL DEFAULT b'0',
	`ismagic` BIT(1) NULL DEFAULT b'0',
	`isemote` BIT(1) NULL DEFAULT b'0',
	`isadvancement` BIT(1) NULL DEFAULT b'0',
	`isappraisal` BIT(1) NULL DEFAULT b'0',
	`isspellcasting` BIT(1) NULL DEFAULT b'0',
	`isallegiance` BIT(1) NULL DEFAULT b'0',
	`isfellowhip` BIT(1) NULL DEFAULT b'0',
	`iscombatenemy` BIT(1) NULL DEFAULT b'0',
	`isrecall` BIT(1) NULL DEFAULT b'0',
	`iscrafting` BIT(1) NULL DEFAULT b'0',
	PRIMARY KEY (`character_id`, `squelched_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TABLE `character_titles` (
	`character_id` INT(10) UNSIGNED NOT NULL,
	`titles` VARCHAR(4096) NULL DEFAULT NULL,
	PRIMARY KEY (`character_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

CREATE TABLE `character_friends` (
	`character_id` INT(11) UNSIGNED NOT NULL,
	`friend_type` INT(11) UNSIGNED NOT NULL,
	`friend_id` INT(11) UNSIGNED NOT NULL,
	PRIMARY KEY (`character_id`, `friend_type`, `friend_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

CREATE TABLE `character_corpses` (
	`character_id` INT(10) UNSIGNED NOT NULL,
	`corpses` VARCHAR(4096) NULL DEFAULT NULL,
	PRIMARY KEY (`character_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

CREATE TABLE `character_windowdata` (
	`character_id` INT(11) NOT NULL DEFAULT '0',
	`bloblength` INT(11) NULL DEFAULT NULL,
	`windowblob` BLOB NULL DEFAULT NULL,
	PRIMARY KEY (`character_id`)
)
ENGINE=InnoDB
;

DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `blob_update_weenie`(
    IN weenieId INT(11) UNSIGNED,
    IN topLevelId INT(11) UNSIGNED,
    IN blockId INT(11) UNSIGNED,
    IN weenieData MEDIUMBLOB
)
BEGIN

INSERT INTO weenies (id, top_level_object_id, block_id, DATA)
    VALUES (weenieId, topLevelId, blockId, weenieData)
    ON DUPLICATE KEY UPDATE
        top_level_object_id=topLevelId,
        block_id=blockId,
        DATA=weenieData;
END //
DELIMITER ;

DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `blob_update_house`(
    IN houseId INT(11),
    IN houseData LONGBLOB
)
BEGIN

INSERT INTO houses (house_id, DATA)
    VALUES (houseId, houseData)
    ON DUPLICATE KEY UPDATE
        DATA=houseData;
END //
DELIMITER ;
